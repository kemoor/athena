# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

from AthenaCommon.Logging import logging

from TrigEDMConfig.TriggerEDM import *   # noqa

log = logging.getLogger( 'TriggerEDM.py' )
log.info("Please import TriggerEDM.py from its new location: Trigger/TriggerCommon/TrigEDMConfig")
